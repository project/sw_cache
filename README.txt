Introduction
-----------
Service Worker Cache is a simple, PHP-based drupal module that provides an automated configurable scripting methodology that your browser runs in the background, separate from a web page, opening the door to features that don't need a web page or user interaction. 
It allows you to support Off-line experiences with an advanced caching mechanism from the Browser end through an extensive use of Javascript-Promises and serve website pages in off-line mode on its own without the need to pull from a central database.

  - This module provides an admin configuration section,
    where the user can select different kind of SW caching mechanism based on their requirement.
  - The module then adds the required Javascript Promises and registers the service worker for  
    the desired caching behavior.
  - The registered Service worker then is terminated when not in use, and restarted when it's next
    needed.
  - Service workers are currently supported by Chrome, Firefox and Opera. Microsoft Edge is now
    showing public support .
    You may visit "https://jakearchibald.github.io/isserviceworkerready/" to follow the progress of all the supported browsers at Jake Archibald's is Serviceworker ready site.
  - Service workers are restricted to running across HTTPS for security reasons.you can only register service
    workers on pages served over HTTPS, so whatever thing browser receives through service worker hasn't been tampered with during its journey through the network

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire service worker cache(sw_cache) directory to the
   Drupal "sites/all/modules" directory.

2. Login as an administrator. 
   Enable the module in the "Administer" -> "Modules" -> "Service Worker Cache".

3. (Optional) Edit the settings under ::
    1. "Administer" -> "Modules" -> "Service Worker Cache" -> "Configure".
    OR
    2. "Administer" -> "Configuration" -> "System" -> "Service Worker Cache Settings".
    OR
    3. Directly go to -> "admin/config/system/swcache-settings".

Maintainers
-----------
Current maintainers:
 * Monoj Nath - https://www.drupal.org/u/monojnath
 * Kaustav Ghosh - https://www.drupal.org/u/only4kaustav
