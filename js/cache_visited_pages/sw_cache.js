/**
 * @file
 * CACHE ONLY VISITED PAGES.
 */

const base_url = new URL(location).searchParams.get('base_url');

const exclude_urls = new URL(location).searchParams.get('exclude_urls').split('||');

const cacheName = new URL(location).searchParams.get('cache_version');

// Call Install Event.
self.addEventListener('install', e => {
  console.log('Service Worker: Installed');
  e.waitUntil(
    caches
      .open(cacheName)
      .then(() => self.skipWaiting())
  );

});

// Call Activate Event.
self.addEventListener('activate', e => {
  console.log('Service Worker: Activated');
  // Remove unwanted caches.
  e.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cache => {
          if (cache !== cacheName) {
            console.log('Service Worker: Clearing Old Cache');
            return caches.delete(cache).then(function () {
              console.log("Service Worker: Cache with name " + cache + " is deleted");
            });
          }
        })
      );
    })
  );
});

// Regex main matcher method.
function matchRuleShort(str, rule) {
  return new RegExp("^" + rule.split("*").join(".*") + "$").test(str);
}

// Regex Wild card matcher.
function matchRuleExpl(str, rule) {
  // "."  => Find a single character, except newline or line terminator
  // ".*" => Matches any string that contains zero or more characters.
  rule = rule.split("*").join(".*");

  // "^"  => Matches any string with the following at the beginning of it
  // "$"  => Matches any string with that in front at the end of it.
  rule = "^" + rule + "$"

  // Create a regular expression object for matching string.
  var regex = new RegExp(rule);

  // Returns true if it finds a match, otherwise it returns false.
  return regex.test(str);
}

// Call Fetch Event.
self.addEventListener('fetch', e => {
  console.log('Service Worker: Fetching');
  e.respondWith(
    fetch(e.request)
      .then(res => {
        var request_url = res.url;
        var request_url = request_url.replace(base_url, "").replace(/^\/|\/$/g, '');
        var is_exclude_url = 'no';
        var arrayLength = exclude_urls.length;
        for (var i = 0; i < arrayLength; i++) {
          if (matchRuleShort(request_url, exclude_urls[i])) {
            is_exclude_url = 'yes';
            break;
          }
        }
        if (is_exclude_url == 'no') {
          // Make copy/clone of response.
          const resClone = res.clone();
          // Open cahce.
          caches.open(cacheName).then(cache => {
            // Add response to cache.
            cache.put(e.request, resClone);
          });
        }
        return res;
      })
      .catch(err => caches.match(e.request).then(res => res))
  );
});
