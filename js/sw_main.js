/**
 * @file
 * Main page to invoke service worker.
 */

Drupal.behaviors.sw_cache = {
  attach:function () {
    // Make sure sw are supported.
    if ('serviceWorker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker
          .register(Drupal.settings.sw_cache.sw_js)
          .then(reg => {
            console.log('Service Worker: Registered (Pages)');
          })
          .catch(err => console.log(`Service Worker: Error: ${err}`));
      });
    }
  }
};
