/**
 * @file
 * CACHE OFFLINE FOR ALL.
 */

const cacheName = new URL(location).searchParams.get('cache_version');

var offline_url = new URL(location).searchParams.get('offline_url');

const offline_assets = new URL(location).searchParams.get('offline_assets').split('||');

const cacheOfflineAssets = [offline_url].concat(offline_assets);

// Call Install Event.
self.addEventListener('install', e => {
  console.log('Service Worker: Installed');
  e.waitUntil(
    caches
      .open(cacheName)
      .then(cache => {
        console.log('Service Worker: Caching Files');
        cache.addAll(cacheOfflineAssets);
      })
      .then(() => self.skipWaiting())
  );
});

// Call Activate Event.
self.addEventListener('activate', e => {
  console.log('Service Worker: Activated');
  // Remove unwanted caches.
  e.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cache => {
          if (cache !== cacheName) {
            console.log('Service Worker: Try to Clear Old Cache');
            return caches.delete(cache).then(function () {
              console.log("Service Worker: Cache with name " + cache + " is deleted");
            });
          }
        })
      );
    })
  );
});

// Call Fetch Event.
self.addEventListener('fetch', e => {
  console.log('Service Worker: Fetching');
  e.respondWith(fetch(e.request)
    .catch((err) => {
      return caches.match(e.request).then(
        (res) => {
          if (res) {
            return res;
          }
          else {
            return caches.match(offline_url);
          }
        }
      )
    })
  );
});
