<?php

/**
 * @file
 * Admin page callbacks for the browser settings configuration form.
 */

/**
 * Form constructor for the browser settings configuration.
 */
function sw_cache_settings_form($form, &$form_state) {

  if (!variable_get('sw_cache_version', FALSE)) {
    $form['sw_cache_initial_information'] = array(
      '#markup' => '<div style="border:5px dashed #ccc;padding:10px;text-align:center;font-size:medium;">Please save your desired configurations in order to work the Service Worker Caching mechanism</div>',

    );

  }
  $form['sw_cache_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Service Worker Cache settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['sw_cache_settings']['sw_cache_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<b>Enable Service Worker Cache</b>'),
    '#description' => t('Enables/Disables Service Worker Cache Feature<br/>.<b>*Note::</b>The site should have a valid SSL certificate for the service worker to work.'),
    '#default_value' => variable_get('sw_cache_enabled', 1),
    '#required' => FALSE,
  );
  if (variable_get('sw_cache_version', FALSE)) {
    $form['sw_cache_settings']['sw_cache_version'] = array(
      '#type' => 'textfield',
      '#title' => t('Service Worker Current Cache Version'),
      '#description' => t("The Service Worker Current Cache Version to be used for caching"),
      '#default_value' => variable_get('sw_cache_version', ''),
      '#required' => FALSE,
      '#disabled' => TRUE,
    );
  }
  $options = array(
    'cache_visited_pages_with_offline' => t('Cache Visited Pages with rest as offline version'),
    'cache_visited_pages' => t('Cache Visited Pages only(ie. Without the offline version)'),
    'cache_offline_for_all' => t('Cache Offline for All page'),
  );
  $form['sw_cache_settings']['sw_cache_type'] = array(
    '#type' => 'radios',
    '#title' => t('Service Worker Cache Type'),
    '#options' => $options,
    '#required' => TRUE,
  );
  if (variable_get('sw_cache_type', FALSE)) {
    $form['sw_cache_settings']['sw_cache_type']['#default_value'] = variable_get('sw_cache_type', '');
  }
  $form['sw_cache_settings']['sw_cache_offline_html_path'] = array(
    '#type' => 'textfield',
    '#title' => t("Add custom Offline html path"),
    '#description' => t("The custom Offline html path."),
    '#size' => 60,
    '#default_value' => variable_get('sw_cache_offline_html_path', ('/' . drupal_get_path('module', 'sw_cache') . '/offline_template/sw_cache.offline.html')),
    '#required' => FALSE,
    '#states' => array(
      "visible" => array(
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_visited_pages_with_offline',
          ),
        ),
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_offline_for_all',
          ),
        ),
      ),
    ),
  );
  $default_offline_assets = (drupal_get_path('module', 'sw_cache') . '/css/style.css') . "\n" . (drupal_get_path('module', 'sw_cache') . '/images/offline.png');
  $form['sw_cache_settings']['sw_cache_offline_assets'] = array(
    '#type' => 'textarea',
    '#title' => t("Add Offline Asset paths to cache"),
    '#description' => t("The Offline Asset paths<br/>i.e. Style paths, Imegae paths etc.<br/>ex. <br/>/sites/all/modules/custom/sw_cache/css/style.css<br/>/sites/all/modules/custom/sw_cache/images/offline.png"),
    '#default_value' => variable_get('sw_cache_offline_assets', $default_offline_assets),
    '#required' => FALSE,
    '#states' => array(
      "visible" => array(
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_visited_pages_with_offline',
          ),
        ),
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_offline_for_all',
          ),
        ),
      ),
    ),
  );

  $form['sw_cache_settings']['sw_cache_exclude_urls'] = array(
    '#type' => 'textarea',
    '#title' => t("Add node url's/paths to exclude from Service Worker Caching."),
    '#description' => t("Add node url's separated by new line for multiple nodes."),
    '#default_value' => variable_get('sw_cache_exclude_urls', ''),
    '#required' => FALSE,
    '#states' => array(
      "visible" => array(
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_visited_pages_with_offline',
          ),
        ),
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_visited_pages',
          ),
        ),
        array(
          "input[name='sw_cache_type']" => array(
            "value" => 'cache_offline_for_all',
          ),
        ),
      ),
    ),
  );

  $form['#submit'][] = 'sw_cache_settings_form_custom_submit';
  return system_settings_form($form);
}

/**
 * Function to copy the sw js file to sites docroot for access to full site.
 */
function sw_cache_settings_form_custom_submit($form, &$form_state) {
  if (!variable_get('sw_cache_version', FALSE) && isset($form_state['input']['sw_cache_type']) && !empty($form_state['input']['sw_cache_type'])) {
    variable_set('sw_cache_version', 'SW_Cache_' . date('YmdHis'));
  }

  $sw_cache_exclude_urls = trim($form_state['input']['sw_cache_exclude_urls']);
  if (!empty($sw_cache_exclude_urls)) {
    $sw_cache_exclude_urls = drupal_strtolower($sw_cache_exclude_urls);
    $sw_cache_exclude_urls = array_filter(array_map('trim', explode("\n", $sw_cache_exclude_urls)));
    foreach ($sw_cache_exclude_urls as &$val) {
      $val = trim($val, '/');
    }
    variable_set('sw_cache_exclude_urls_refined', implode('||', $sw_cache_exclude_urls));
  }
  $form_state['values']['sw_cache_exclude_urls'] = implode("\n", $sw_cache_exclude_urls);

  $source = DRUPAL_ROOT . '/' . drupal_get_path('module', 'sw_cache');
  $offline_source = $source . '/js/cache_visited_pages_with_offline/sw_cache.js';
  switch ($form_state['input']['sw_cache_type']) {
    case 'cache_visited_pages':
      $offline_source = $source . '/js/cache_visited_pages/sw_cache.js';
    case 'cache_visited_pages_with_offline':
      $sw_cache_offline_html_path = trim($form_state['input']['sw_cache_offline_html_path'], '/');
      $sw_cache_offline_html_path = ($sw_cache_offline_html_path) ? ('/' . $sw_cache_offline_html_path) : ('/' . drupal_get_path('module', 'sw_cache') . '/offline.html');
      $form_state['values']['sw_cache_offline_html_path'] = $sw_cache_offline_html_path;

      $cache_asset_paths = array_filter(array_map('trim', explode("\n", $form_state['input']['sw_cache_offline_assets'])));
      foreach ($cache_asset_paths as &$path) {
        $path = trim($path, '/');
        $path = '/' . $path;
      }
      $cache_asset_paths = implode("\n", $cache_asset_paths);
      $form_state['values']['sw_cache_offline_assets'] = $cache_asset_paths;
      $source = $offline_source;
      break;

    case 'cache_offline_for_all':
      $source .= '/js/cache_offline_for_all/sw_cache.js';
      break;
  }
  $destination = DRUPAL_ROOT;
  file_unmanaged_copy($source, $destination, FILE_EXISTS_REPLACE);
}
